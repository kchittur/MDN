# Mini-batch gradient descent on 4-vectors from lepton and J/psi


import tensorflow as tf
import numpy as np
import pandas as pd
import math
from tensorflow.python import debug as tf_debug
from scipy import integrate

n_hidden_1 = 10
n_hidden_2 = 10
k_mix = 1 # Creates k_mix bernstein polynomial(s) (for each two 4-vector input)
n_out = 4 * k_mix
std = 0.1

# Input/Output placeholders
x = tf.placeholder(dtype=tf.float64, shape=[None,8], name='x')
y = tf.placeholder(dtype=tf.float64, shape=[None,1], name='y')

# Specify variables for hidden layer
Wh_1 = tf.Variable(tf.random_normal([8,n_hidden_1], stddev=std, dtype=tf.float64))
bh_1 = tf.Variable(tf.random_normal([1,n_hidden_1], stddev=std, dtype=tf.float64))

Wh_2 =  tf.Variable(tf.random_normal([n_hidden_1,n_hidden_2], stddev=std, dtype=tf.float64))
bh_2 = tf.Variable(tf.random_normal([1,n_hidden_2], stddev=std, dtype=tf.float64))

#Specify variables in output layer
Wo = tf.Variable(tf.random_normal([n_hidden_2,n_out], stddev=std, dtype=tf.float64))
bo = tf.Variable(tf.random_normal([1,n_out],stddev=std, dtype=tf.float64))

#Specify architecture
hidden_layer_1 = tf.nn.relu(tf.matmul(x,Wh_1)+bh_1)
hidden_layer_2 = tf.nn.relu(tf.matmul(hidden_layer_1,Wh_2)+bh_2)
out_layer = tf.matmul(hidden_layer_2,Wo)+bo


# Get a subsample of the data
def next_batch(batch_size,info,labels):
    dim = labels.size
    perm = np.arange(dim)
    np.random.shuffle(perm)
    info = info[perm]
    labels = labels[perm]
    labels = np.reshape(labels,(dim,1))
    return info[0:batch_size], labels[0:batch_size]


#Integrate Bernstein polynomial
# a,b,c,d are the coefficients of the Bernstein polynomial
# low and high are the bounds of the integral
def my_int(a,b,c,d,low,high):
    upper0 = a*(-0.25*tf.pow(1.0-high,4.0))
    upper1 = b*(3.0*(0.25*tf.pow(high,4.0)-2.0*(tf.pow(high,3.0)/3.0)+0.5*tf.pow(high,2.0)))
    upper2 = c*(3.0*((tf.pow(high,3.0)/3.0)-0.25*tf.pow(high,4.0)))
    upper3 = d*0.25*tf.pow(high,4.0)

    lower0 = a*(-0.25*tf.pow(1.0-low,4.0))
    lower1 = b*(3.0*(0.25*tf.pow(low,4.0)-2.0*(tf.pow(low,3.0)/3.0)+0.5*tf.pow(low,2.0)))
    lower2 = c*(3.0*((tf.pow(low,3.0)/3.0)-0.25*tf.pow(low,4.0)))
    lower3 = d*0.25*tf.pow(low,4.0)

    upper = upper0+upper1+upper2+upper3
    lower = lower0+lower1+lower2+lower3

    norm = upper-lower
    return norm

# Gives mixture coefficient, mean, and stddev from output of neural net
def get_vals(output):
    out_a = tf.placeholder(dtype=tf.float64, shape=[None,k_mix])
    out_b = tf.placeholder(dtype=tf.float64, shape=[None,k_mix])
    out_c = tf.placeholder(dtype=tf.float64, shape=[None,k_mix])
    out_d = tf.placeholder(dtype=tf.float64, shape=[None,k_mix])

    out_a, out_b, out_c, out_d = tf.split(output, 4, axis=1) 


    [out_a,out_b,out_c,out_d] = [tf.nn.sigmoid(x) for x in [out_a,out_b,out_c,out_d]]
    #[out_a,out_b,out_c,out_d] = [tf.nn.relu(x) for x in [out_a,out_b,out_c,out_d]]
    
    low = 0.0
    high = 1.0 

    # Normalize polynomial in order to represent a PDF
    low = tf.constant(low,dtype=tf.float64)
    high = tf.constant(high,dtype=tf.float64)
    norm = my_int(out_a,out_b,out_c,out_d,low,high)
    out_a,out_b,out_c,out_d = [x/norm for x in [out_a,out_b,out_c,out_d]]

    return out_a, out_b, out_c, out_d

out_a, out_b, out_c, out_d = get_vals(out_layer)


# Get data as numpy arrays
# Curently df_0 has data for top mass of 170.0 GeV
#          df_1 has data for top mass of 175.5 GeV
df_0 = pd.read_csv("data/data_0_truth_300.dat", sep = ',', dtype=np.float64)
df_1 = pd.read_csv("data/data_1_truth_300.dat", sep = ',', dtype=np.float64)
#df_2 = pd.read_csv("data/data_2_truth.dat", sep = ',', dtype=np.float64)
#df_3 = pd.read_csv("data/data_3_truth.dat", sep = ',', dtype=np.float64)
#df_4 = pd.read_csv("data/data_4_truth.dat", sep = ',', dtype=np.float64)


arr_0 = df_0.as_matrix()
arr_1 = df_1.as_matrix()
arr_0_len = (arr_0.shape)[0]
arr_1_len = (arr_1.shape)[0]

# Make sure we have the same number of events for both data sets
if(arr_0_len > arr_1_len):
    arr_0 = arr_0[0:arr_1_len,:]
    arr_len = arr_1_len
elif(arr_1_len > arr_0_len):
    arr_1 = arr_1[0:arr_0_len,:]
    arr_len = arr_0_len
else:
    arr_len = arr_0_len

# Stack and shuffle the data
arr = [arr_0,arr_1]
dat = np.vstack(arr)
np.random.shuffle(dat)


# Split data: 2/3 (of combined data) goes to training, 1/3 goes to testing
# Columns of data are: top_mass, m_total, Jpsi_E, Jpsi_px, Jpsi_py, Jpsi_pz, l_E, l_px, l_py, l_pz, is_elec
# Where m_total is m(l,J/Psi) and is_elec states whether the lepton is an electron
#Input to neural net is columns 2 through 9 (inclusive). Output is column 0.
train_len = int ((4.0/3.0)*(float (arr_len)))
train_arr = dat[0:train_len,:]
test_arr = dat[train_len:(2*arr_len),:]
x_data,y_data = train_arr[:,2:10],train_arr[:,0]
x_test,y_test = test_arr[:,2:10],test_arr[:,0]
y_data = y_data.reshape(train_len,1)
y_test = y_test.reshape(((2*arr_len)-train_len),1)

#Normalize data
y_data = (y_data-170.0)/5.0
max_train = np.amax(x_data,axis=0)
min_train = np.amin(x_data,axis=0)

x_data = (x_data-min_train)/(max_train-min_train)
x_test = (x_test-min_train)/(max_train-min_train)

#Define cost function
def get_cost(a,b,c,d,y): 
    r0 = a*(tf.pow(1.0-y,3.0))
    r1 = b*(3.0*y*(tf.pow(1.0-y,2.0)))
    r2 = c*(3.0*tf.pow(y,2.0)*(1.0-y))
    r3 = d*tf.pow(y,3.0)

    res = r0+r1+r2+r3
    res = - tf.log(res)
    res = tf.reduce_mean(res)
    return res

cost_func = get_cost(out_a,out_b,out_c,out_d,y)

# Define optimizer on cost function
train_op = tf.train.AdamOptimizer().minimize(cost_func)
#Open session
sess = tf.InteractiveSession()
#Initialize variables
sess.run(tf.global_variables_initializer())

# Following two lines used for debugging
#sess = tf_debug.LocalCLIDebugWrapperSession(sess)
#sess.add_tensor_filter("has_inf_or_nan", tf_debug.has_inf_or_nan)


#Train
n_epochs=100000
loss_arr = np.zeros(n_epochs)
batch_size = 1000
for i in range(n_epochs):
    batch_xs, batch_ys = next_batch(batch_size, x_data,y_data)
    loss = sess.run(cost_func, feed_dict={x: batch_xs, y: batch_ys})
    if i %100 == 0:
        print("Epoch #" + str(i) + ", Loss = " +str(loss))
    loss_arr[i] = loss
np.savetxt("loss_test_300_batch.dat",loss_arr,delimiter=',')

#Test neural net 
test_a, test_b, test_c, test_d = sess.run(get_vals(out_layer),feed_dict={x: x_test})
tot_data = np.hstack((y_test,test_a,test_b,test_c,test_d))
head = "mass,out_a,out_b,out_c,out_d"
np.savetxt("test_output_300_batch_10.dat",tot_data,delimiter=',',header=head,comments='')
