import tensorflow as tf
import numpy as np
import pandas as pd
import math
from tensorflow.python import debug as tf_debug

n_hidden_1 = 10
n_hidden_2 = 10
k_mix = 2
n_out = 3 * k_mix
std = 0.1

x = tf.placeholder(dtype=tf.float64, shape=[None,8], name='x')
y = tf.placeholder(dtype=tf.float64, shape=[None,1], name='y')

# Specify variables for hidden layer
Wh_1 = tf.Variable(tf.random_normal([8,n_hidden_1], stddev=std, dtype=tf.float64))
bh_1 = tf.Variable(tf.random_normal([1,n_hidden_1], stddev=std, dtype=tf.float64))

Wh_2 =  tf.Variable(tf.random_normal([n_hidden_1,n_hidden_2], stddev=std, dtype=tf.float64))
bh_2 = tf.Variable(tf.random_normal([1,n_hidden_2], stddev=std, dtype=tf.float64))

#Specify variables in output layer
Wo = tf.Variable(tf.random_normal([n_hidden_2,n_out], stddev=std, dtype=tf.float64))
bo = tf.Variable(tf.random_normal([1,n_out],stddev=std, dtype=tf.float64))

#Specify architecture
hidden_layer_1 = tf.nn.relu(tf.matmul(x,Wh_1)+bh_1)
hidden_layer_2 = tf.nn.relu(tf.matmul(hidden_layer_1,Wh_2)+bh_2)
out_layer = tf.matmul(hidden_layer_2,Wo)+bo


#Getter function to output mixture coefficient, mean, and stddev
def get_vals(output):
    out_pi = tf.placeholder(dtype=tf.float64, shape=[None,k_mix])
    out_mu = tf.placeholder(dtype=tf.float64, shape=[None,k_mix])
    out_sig = tf.placeholder(dtype=tf.float64, shape=[None,k_mix])

    out_pi, out_sig, out_mu = tf.split(output, 3, axis=1) 

    #Softmax coefficients so the coefficients are normalized
    out_pi = tf.exp(out_pi)
    pi_sum = tf.reduce_sum(out_pi,1,keep_dims=True)
    out_pi = tf.divide(out_pi,pi_sum)
    
    #We integrate each PDF individually
    pi1,pi2,mu1,mu2,sig1,sig2 = [tf.placeholder(dtype=tf.float64,shape=[None])]*6
    mu1,mu2 = tf.split(out_mu,2,axis=1)
    sig1,sig2 = tf.split(out_sig,2,axis=1)
    sig1 = 100.0 * sig1
    sig2 = 100.0 * sig2
    pi1,pi2 = tf.split(out_pi,2,axis=1)
    dist1 = tf.contrib.distributions.Normal(loc=mu1,scale=sig1)
    dist2 = tf.contrib.distributions.Normal(loc=mu2,scale=sig2)

    #HARDCODED IN MEAN AND STD
    g_mean=173.5
    g_std=2.62678510731
    low = (170.0-g_mean)/g_std
    high = (177.5-g_mean)/g_std


    low = tf.constant(low,dtype=tf.float64)
    high = tf.constant(high,dtype=tf.float64)


    dist1_lower = dist1.cdf(low)
    dist2_lower = dist2.cdf(low)
    dist1_upper = dist1.cdf(high)
    dist2_upper = dist2.cdf(high)

    ''' 
    dist1_lower = tf.Print(dist1_lower,[dist1_lower],message='dist1_lower= ')
    dist1_upper = tf.Print(dist1_upper,[dist1_upper],message='dist1_upper= ')
    dist2_lower = tf.Print(dist2_lower,[dist2_lower],message='dist2_lower= ')
    dist2_upper = tf.Print(dist2_upper,[dist2_upper],message='dist2_upper= ')
   '''

    norm1 = -1.0 * (dist1_upper - dist1_lower)
    norm2 = -1.0 * (dist2_upper - dist2_lower)

    norm_min = 0.001 * tf.ones_like(norm1)
    norm_max = 0.999 * tf.ones_like(norm1)  

    norm1 = tf.clip_by_value(norm1,norm_min,norm_max)
    norm2 = tf.clip_by_value(norm2,norm_min,norm_max)  

    norms = tf.concat([norm1,norm2],1)
    out_pi = tf.divide(out_pi,norms)
    

    #Exponentiate sigmas so that they are positive
    out_sig = tf.exp(out_sig)
    return out_pi, out_sig, out_mu

out_pi, out_sig, out_mu = get_vals(out_layer)


# Get data as numpy arrays
df_0 = pd.read_csv("data/data_0.dat", sep = ',', dtype=np.float64)
df_1 = pd.read_csv("data/data_1.dat", sep = ',', dtype=np.float64)
df_2 = pd.read_csv("data/data_2.dat", sep = ',', dtype=np.float64)
df_3 = pd.read_csv("data/data_3.dat", sep = ',', dtype=np.float64)
df_4 = pd.read_csv("data/data_4.dat", sep = ',', dtype=np.float64)

df = [df_0,df_1,df_2,df_3,df_4]
arr = [a.as_matrix() for a in df]

train_arr = [a[0:10000,:] for a in arr]
test_arr = [a[10000:20000,:] for a in arr]
train_arr = np.vstack(train_arr)
test_arr = np.vstack(test_arr)
x_data,y_data = train_arr[:,1:10],train_arr[:,0]
x_test,y_test = test_arr[:,1:10],test_arr[:,0]
y_data = y_data.reshape(50000,1)
y_test = y_test.reshape(50000,1)

#Normalize data
train_mean = np.mean(x_data,axis=0)
train_std = np.std(x_data,axis=0)
test_mean = np.mean(x_test,axis=0)
test_std = np.std(x_test,axis=0)

label_train_mean = np.mean(y_data)
label_train_std = np.std(y_data)
print(label_train_mean)
print(label_train_std)
#label_test_mean = np.mean(y_test)
#label_test_std = np.std(y_test)

x_data = (x_data-train_mean)/train_std
x_test = (x_test-train_mean)/train_std
y_data = (y_data-label_train_mean)/label_train_std
y_test = (y_test-label_train_mean)/label_train_std

# Get probability from normal dist. to be used in cost function
# Don't worry too much about dimensions. Tensorflow uses broadcasting
def prob_normal(y,mu,sig):
    res = tf.subtract(y,mu)
    res = tf.divide(res,sig)
    res = - tf.square(res)/2
    res = tf.exp(res)
    res = res/ np.sqrt(2*np.pi)
    return tf.divide(res,sig)

#Define cost function
def get_cost(out_pi,out_sig,out_mu,y): 
    res = prob_normal(y,out_mu,out_sig)
    res = tf.multiply(res,out_pi)
    res = tf.reduce_sum(res,1)
    res = - tf.log(res)
    res = tf.reduce_mean(res)
    return res

cost_func = get_cost(out_pi,out_sig,out_mu,y)

# Define optimizer on cost function
train_op = tf.train.AdamOptimizer().minimize(cost_func)
#Open session
sess = tf.InteractiveSession()
#Initialize variables
sess.run(tf.global_variables_initializer())
#sess = tf_debug.LocalCLIDebugWrapperSession(sess)
#sess.add_tensor_filter("has_inf_or_nan", tf_debug.has_inf_or_nan)


#TRAIN!!!!
n_epochs=10000
loss_arr = np.zeros(n_epochs)
for i in range(n_epochs):
    sess.run(train_op,feed_dict={x:x_data, y:y_data})
    loss = sess.run(cost_func, feed_dict={x: x_data, y: y_data})
    if i %10 == 0:
        print("Epoch #" + str(i) + ", Loss = " +str(loss))
    loss_arr[i] = loss
np.savetxt("loss_test.dat",loss_arr,delimiter=',')

#Test out 

pi_test, sig_test, mu_test = sess.run(get_vals(out_layer),feed_dict={x: x_test})
y_test = (y_test*label_train_std)+label_train_mean
mu_1 = (mu_test[:,0]*label_train_std)+label_train_mean
mu_2 = (mu_test[:,1]*label_train_std)+label_train_mean
mu_1=mu_1.reshape(50000,1)
mu_2=mu_2.reshape(50000,1)
tot_data = np.hstack((y_test,pi_test,mu_1,mu_2,sig_test))
head = "mass,pi1,pi2,mu1,mu2,sig1,sig2"
np.savetxt("test_data_clipped.dat",tot_data,delimiter=',',header=head,comments='')

print("WARNING!!!!!!!!!")
print("MEAN AND STD OF TRAINING DATA IS HARDCODED!!!!!!!!!")
