import numpy as np

num = 20000
m_data = np.float32(np.random.uniform(0.0, 5.0, (1, num))).T
p_data = np.float32(np.random.uniform(-10.0, 10.0, (1, num))).T
e_exact = np.sqrt(np.square(m_data) + np.square(p_data))
e_data = e_exact #+ np.float32(np.random.normal(0.0,0.3,(1,num))).T
data = np.hstack((e_data,m_data,p_data))
np.savetxt('fake.dat',data,fmt='%.18f',delimiter=' ')
