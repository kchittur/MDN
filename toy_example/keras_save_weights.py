import numpy as np
import pandas
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.layers.advanced_activations import LeakyReLU
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from keras import backend as K 
from keras.callbacks import ModelCheckpoint,CSVLogger
import sys
import tensorflow as tf


def give_model(bool_layer2,n_hidden1,n_hidden2):
    def deep_model():
        model = Sequential([
        Dense(30, input_dim=2, init='normal', name='dense_a')
        ])

        model.add(LeakyReLU(alpha=0.01))

        if bool_layer2:
            model.add(Dense(30,init='normal',name='dense_b'))
            model.add(LeakyReLU(alpha=0.01))

        model.add(Dense(6, init = 'normal',name='dense_c'))

        model.compile(loss = custom_mdn_loss, optimizer='adam')
        return model

    return deep_model

def s_softmax(x):
    x = tf.exp(x)
    s = tf.reduce_sum(x,axis=1)
    return (x / s)

def s_normal(y,mu,sig):
    res = tf.subtract(y,mu)
    res = tf.divide(res,sig)
    res = -tf.square(res)/2
    res = tf.exp(res)
    res = tf.divide(res,sig)
    res = res / np.sqrt(2*np.pi)
    return res
    
def custom_mdn_loss(y_true,stat_pred):
    y_true = y_true[:,0]
    y_true = tf.stack([y_true,y_true],axis=1)
    co = s_softmax(stat_pred[:,0:2])
    mu = stat_pred[:,2:4]
    sig = tf.exp(stat_pred[:,4:6])
    res = s_normal(y_true,mu,sig)
    res = tf.multiply(res,co)
    res = tf.reduce_sum(res,axis=1)
    res = - tf.log(res)
    return tf.reduce_sum(res)


dataframe = pandas.read_csv(sys.argv[1], header=None,delimiter=' ')
dataset = dataframe.values
X = dataset[:10000,1:]
X = X.reshape((10000,2))
Y = dataset[:10000,0]
Y = Y.reshape((10000,1))
z = np.zeros((10000,1))
Y = np.hstack((Y,z,z))
# Needed to edit true y values to avoid dimension mismatch in custom loss function
seed = 7
np.random.seed(seed)

mod = give_model(True,1,1)

fpath = 'weights.h5'

reg = KerasRegressor(build_fn=mod, epochs=500, batch_size=40, verbose=1)

save_call = ModelCheckpoint(fpath,monitor= 'val_loss',verbose = 0, save_best_only=False,save_weights_only=True,mode='auto')

reg.fit(X,Y,callbacks=[save_call])

