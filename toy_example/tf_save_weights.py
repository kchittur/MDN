import tensorflow as tf
import numpy as np
import pandas as pd
import math
from tensorflow.python import debug as tf_debug

n_hidden = 6
k_mix = 2
n_out = 3 * k_mix
std = 0.5

x = tf.placeholder(dtype=tf.float64, shape=[None,2], name='x')
y = tf.placeholder(dtype=tf.float64, shape=[None,1], name='y')

# Specify variables for hidden layer
Wh = tf.Variable(tf.random_normal([2,n_hidden], stddev=std, dtype=tf.float64))
bh = tf.Variable(tf.random_normal([1,n_hidden], stddev=std, dtype=tf.float64))

#Specify variables in output layer
Wo = tf.Variable(tf.random_normal([n_hidden,n_out], stddev=std, dtype=tf.float64))
bo = tf.Variable(tf.random_normal([1,n_out],stddev=std, dtype=tf.float64))

#Specify architecture
hidden_layer = tf.nn.relu(tf.matmul(x,Wh)+bh)
out_layer = tf.matmul(hidden_layer,Wo)+bo

#Getter function to output mixture coefficient, mean, and stddev
def get_vals(output):
    out_pi = tf.placeholder(dtype=tf.float64, shape=[None,k_mix])
    out_mu = tf.placeholder(dtype=tf.float64, shape=[None,k_mix])
    out_sig = tf.placeholder(dtype=tf.float64, shape=[None,k_mix])

    out_pi, out_sig, out_mu = tf.split(output, 3, axis=1) 

    #Softmax coefficients so they are normalized
    out_pi = tf.exp(out_pi)
    pi_sum = tf.reduce_sum(out_pi,1,keep_dims=True)
    out_pi = tf.divide(out_pi,pi_sum)

    #Exponentiate sigmas so that they are positive
    out_sig = tf.exp(out_sig)
    return out_pi, out_sig, out_mu

out_pi, out_sig, out_mu = get_vals(out_layer)


# Get data as numpy arrays
df = pd.read_csv("fake.dat", sep = ' ',names=["e","m","p"], dtype=np.float64)
arr = df.as_matrix()
train_arr = arr[0:10000,:]
x_data,y_data = np.split(train_arr,[2],axis=1)
test_arr = arr[10000:20000,:]
x_test,y_test = np.split(test_arr,[2],axis=1)


# Get probability from normal dist. to be used in cost function
# Don't worry too much about dimensions. Tensorflow uses broadcasting
def prob_normal(y,mu,sig):
    res = tf.subtract(y,mu)
    res = tf.divide(res,sig)
    res = - tf.square(res)/2
    res = tf.exp(res)
    res = res/ np.sqrt(2*np.pi)
    return tf.divide(res,sig)

#Define cost function
def get_cost(out_pi,out_sig,out_mu,y):     
    res = prob_normal(y,out_mu,out_sig)
    res = tf.multiply(res,out_pi)
    res = tf.reduce_sum(res,1)
    res = - tf.log(res)
    res = tf.reduce_mean(res)
    return res
cost_func = get_cost(out_pi,out_sig,out_mu,y)

# Define optimizer on cost function
train_op = tf.train.AdamOptimizer().minimize(cost_func)
#Open session
sess = tf.InteractiveSession()

# Below two lines for debugging
#sess = tf_debug.LocalCLIDebugWrapperSession(sess)
#sess.add_tensor_filter("has_inf_or_nan", tf_debug.has_inf_or_nan)


#Train
n_epochs=2500
loss_arr = np.zeros(n_epochs)
for i in range(n_epochs):
    print("Epoch #"+str(i))
    sess.run(train_op,feed_dict={x:x_data, y:y_data})
    loss_arr[i] = sess.run(cost_func, feed_dict={x: x_data, y: y_data})

#Test 
pi_test, sig_test, mu_test = sess.run(get_vals(out_layer),feed_dict={x: x_test})
tot_data = np.hstack((x_test,y_test,pi_test,mu_test,sig_test))
head = "e,m,p,pi1,pi2,mu1,mu2,sig1,sig2"
np.savetxt("test_data_ne.dat",tot_data,delimiter=',',header=head,comments='')
