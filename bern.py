import tensorflow as tf
import numpy as np
import pandas as pd
import math
from tensorflow.python import debug as tf_debug
from scipy import integrate

n_hidden_1 = 10
n_hidden_2 = 10
k_mix = 1
n_out = 4 * k_mix
std = 0.1

x = tf.placeholder(dtype=tf.float64, shape=[None,8], name='x')
y = tf.placeholder(dtype=tf.float64, shape=[None,1], name='y')

# Specify variables for hidden layer
Wh_1 = tf.Variable(tf.random_normal([8,n_hidden_1], stddev=std, dtype=tf.float64))
bh_1 = tf.Variable(tf.random_normal([1,n_hidden_1], stddev=std, dtype=tf.float64))

Wh_2 =  tf.Variable(tf.random_normal([n_hidden_1,n_hidden_2], stddev=std, dtype=tf.float64))
bh_2 = tf.Variable(tf.random_normal([1,n_hidden_2], stddev=std, dtype=tf.float64))

#Specify variables in output layer
Wo = tf.Variable(tf.random_normal([n_hidden_2,n_out], stddev=std, dtype=tf.float64))
bo = tf.Variable(tf.random_normal([1,n_out],stddev=std, dtype=tf.float64))

#Specify architecture
hidden_layer_1 = tf.nn.relu(tf.matmul(x,Wh_1)+bh_1)
hidden_layer_2 = tf.nn.relu(tf.matmul(hidden_layer_1,Wh_2)+bh_2)
out_layer = tf.matmul(hidden_layer_2,Wo)+bo

#Integrate Bernstein polynomial
def my_int(a,b,c,d,low,high):
    upper0 = a*(-0.25*tf.pow(1.0-high,4.0))
    upper1 = b*(3.0*(0.25*tf.pow(high,4.0)-2.0*(tf.pow(high,3.0)/3.0)+0.5*tf.pow(high,2.0)))
    upper2 = c*(3.0*((tf.pow(high,3.0)/3.0)-0.25*tf.pow(high,4.0)))
    upper3 = d*0.25*tf.pow(high,4.0)

    lower0 = a*(-0.25*tf.pow(1.0-low,4.0))
    lower1 = b*(3.0*(0.25*tf.pow(low,4.0)-2.0*(tf.pow(low,3.0)/3.0)+0.5*tf.pow(low,2.0)))
    lower2 = c*(3.0*((tf.pow(low,3.0)/3.0)-0.25*tf.pow(low,4.0)))
    lower3 = d*0.25*tf.pow(low,4.0)

    upper = upper0+upper1+upper2+upper3
    lower = lower0+lower1+lower2+lower3

    #upper = tf.Print(upper,[upper],message="upper = ")
    #lower = tf.Print(lower,[lower],message="lower = ")

    norm = upper-lower
    return norm

#Getter function to output mixture coefficient, mean, and stddev
def get_vals(output):
    out_a = tf.placeholder(dtype=tf.float64, shape=[None,k_mix])
    out_b = tf.placeholder(dtype=tf.float64, shape=[None,k_mix])
    out_c = tf.placeholder(dtype=tf.float64, shape=[None,k_mix])
    out_d = tf.placeholder(dtype=tf.float64, shape=[None,k_mix])

    out_a, out_b, out_c, out_d = tf.split(output, 4, axis=1) 


    [out_a,out_b,out_c,out_d] = [tf.nn.sigmoid(x) for x in [out_a,out_b,out_c,out_d]]
    #[out_a,out_b,out_c,out_d] = [tf.nn.relu(x) for x in [out_a,out_b,out_c,out_d]]
    low = 0.0
    high = 1.0 


    low = tf.constant(low,dtype=tf.float64)
    high = tf.constant(high,dtype=tf.float64)

    norm = my_int(out_a,out_b,out_c,out_d,low,high)
    #norm = tf.Print(norm,[norm],message="norm = ")
    out_a,out_b,out_c,out_d = [x/norm for x in [out_a,out_b,out_c,out_d]]

    '''
    out_a = tf.Print(out_a,[out_a],message = "a = ")
    out_b = tf.Print(out_b,[out_b],message= " b = ")
    out_c = tf.Print(out_c,[out_c],message="c = ")
    out_d = tf.Print(out_d,[out_d],message="d = ")
    '''
    

    return out_a, out_b, out_c, out_d

out_a, out_b, out_c, out_d = get_vals(out_layer)


# Get data as numpy arrays
df_0 = pd.read_csv("data/data_0_truth_300.dat", sep = ',', dtype=np.float64)
df_1 = pd.read_csv("data/data_1_truth_300.dat", sep = ',', dtype=np.float64)
#df_2 = pd.read_csv("data/data_2_truth.dat", sep = ',', dtype=np.float64)
#df_3 = pd.read_csv("data/data_3_truth.dat", sep = ',', dtype=np.float64)
#df_4 = pd.read_csv("data/data_4_truth.dat", sep = ',', dtype=np.float64)


arr_0 = df_0.as_matrix()
arr_1 = df_1.as_matrix()
arr_0_len = (arr_0.shape)[0]
arr_1_len = (arr_1.shape)[0]

if(arr_0_len > arr_1_len):
    arr_0 = arr_0[0:arr_1_len,:]
    arr_len = arr_1_len
elif(arr_1_len > arr_0_len):
    arr_1 = arr_1[0:arr_0_len,:]
    arr_len = arr_0_len
else:
    arr_len = arr_0_len

arr = [arr_0,arr_1]

dat = np.vstack(arr)
np.random.shuffle(dat)

train_len = int ((4.0/3.0)*(float (arr_len)))
#top_mass, m_total, Jpsi_E, Jpsi_px, Jpsi_py, Jpsi_pz, l_E, l_px, l_py, l_pz, is_elec
train_arr = dat[0:train_len,:]
test_arr = dat[train_len:(2*arr_len),:]
x_data,y_data = train_arr[:,2:10],train_arr[:,0]
x_test,y_test = test_arr[:,2:10],test_arr[:,0]
y_data = y_data.reshape(train_len,1)
y_test = y_test.reshape(((2*arr_len)-train_len),1)

#Normalize data
'''
train_mean = np.mean(x_data,axis=0)
train_std = np.std(x_data,axis=0)
test_mean = np.mean(x_test,axis=0)
test_std = np.std(x_test,axis=0)

label_train_mean = np.mean(y_data)
label_train_std = np.std(y_data)
#print(label_train_mean)
#print(label_train_std)
#label_test_mean = np.mean(y_test)
#label_test_std = np.std(y_test)

x_data = (x_data-train_mean)/train_std
x_test = (x_test-train_mean)/train_std
y_data = (y_data-label_train_mean)/label_train_std
y_test = (y_test-label_train_mean)/label_train_std
'''
y_data = (y_data-170.0)/5.0
#y_test = (y_test-170.0)/7.5
max_train = np.amax(x_data,axis=0)
min_train = np.amin(x_data,axis=0)

x_data = (x_data-min_train)/(max_train-min_train)
x_test = (x_test-min_train)/(max_train-min_train)

#Define cost function
def get_cost(a,b,c,d,y): 
    r0 = a*(tf.pow(1.0-y,3.0))
    r1 = b*(3.0*y*(tf.pow(1.0-y,2.0)))
    r2 = c*(3.0*tf.pow(y,2.0)*(1.0-y))
    r3 = d*tf.pow(y,3.0)



    #[r0,r1,r2,r3] = [tf.nn.relu(x) for x in [r0,r1,r2,r3]]
    res = r0+r1+r2+r3
    #res = tf.Print(res,[res],message="res = ")
    res = - tf.log(res)
    #res = tf.Print(res,[res],message="res = ")
    res = tf.reduce_mean(res)
    return res

cost_func = get_cost(out_a,out_b,out_c,out_d,y)

# Define optimizer on cost function
train_op = tf.train.AdamOptimizer().minimize(cost_func)
#Open session
sess = tf.InteractiveSession()
#Initialize variables
sess.run(tf.global_variables_initializer())
#sess = tf_debug.LocalCLIDebugWrapperSession(sess)

#sess.add_tensor_filter("has_inf_or_nan", tf_debug.has_inf_or_nan)


#TRAIN!!!!
n_epochs=50000
loss_arr = np.zeros(n_epochs)
for i in range(n_epochs):
    sess.run(train_op,feed_dict={x:x_data, y:y_data})
    loss = sess.run(cost_func, feed_dict={x: x_data, y: y_data})
    if i %10 == 0:
        print("Epoch #" + str(i) + ", Loss = " +str(loss))
    loss_arr[i] = loss
np.savetxt("loss_test.dat",loss_arr,delimiter=',')

#Test out 
test_a, test_b, test_c, test_d = sess.run(get_vals(out_layer),feed_dict={x: x_test})
tot_data = np.hstack((y_test,test_a,test_b,test_c,test_d))
head = "mass,out_a,out_b,out_c,out_d"
np.savetxt("test_output_300.dat",tot_data,delimiter=',',header=head,comments='')
