import ROOT
from math import cos, sin, sinh, sqrt
import os.path

file_ind = [410037, 410038, 410039, 410040, 410041]
resp_top_mass = [170.0,171.5,173.5,175.0,177.5]

for d_ind in range(5):

    num_elec = 0
    num_muon = 0
    out_file = open("data_"+str(d_ind)+"_reco.dat","w")
    out_file.write("top_mass, m_total, Jpsi_E, Jpsi_px, Jpsi_py, Jpsi_pz, l_E, l_px, l_py, l_pz, is_elec\n")


    d = file_ind[d_ind]
    for f_path in os.listdir("/data_ceph/cburton/ntuples/"+str(d)):
        if not f_path.endswith(".root"):
            continue
        f = ROOT.TFile(f_path)
        t = f.Get("nominal")



        for e in t:
            
            top_mass = resp_top_mass[d_ind]

            px_J, py_J = e.Jpsi_Pt[0]*cos(e.Jpsi_Phi[0]),  e.Jpsi_Pt[0]*sin(e.Jpsi_Phi[0])
            pz_J = e.Jpsi_Pt[0]*sinh(e.Jpsi_Eta[0])
            E_J = e.Jpsi_E[0]

            if len(e.electron_eta) == 1:
                px_l, py_l = e.electron_pt[0]*cos(e.electron_phi[0]),  e.electron_pt[0]*sin(e.electron_phi[0])
                pz_l = e.electron_pt[0]*sinh(e.electron_eta[0])
                E_l = e.electron_E[0]
                is_electron = True
                num_elec += 1
            elif len(e.muon_eta) == 2:
                px_l, py_l = e.muon_pt[0]*cos(e.muon_phi[0]),  e.muon_pt[0]*sin(e.muon_phi[0])
                pz_l = e.muon_pt[0]*sinh(e.muon_eta[0])
                E_l = e.muon_E[0]
                is_electron = False
                num_muon += 1
            else:
                continue
            p_total_sq = (px_J+px_l)**2 + (py_J+py_l)**2 + (pz_J+pz_l)**2
            m_total = (E_J+E_l)**2 + p_total_sq
            m_total = sqrt(m_total)


            out_file.write("%f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f\n" %(top_mass,m_total, E_J,px_J,py_J,pz_J,E_l,px_l,py_l,pz_l,is_electron))
    out_file.close()
    print("Top mass: %f, electrons: %d, muons: %d\n" %(resp_top_mass[d_ind],num_elec,num_muon))

