import ROOT
from math import cos, sin, sinh, cosh, sqrt
import os
ROOT.xAOD.Init().isSuccess()

def lepton_from_Jpsi(parent_list,m_truth_pdgId,m_truth_barcode):
    for parent_barcode in parent_list:
        for truth_barcode,truth_pdgId in zip(m_truth_barcode,m_truth_pdgId):
            if truth_barcode==parent_barcode and truth_pdgId==443:
                return True
            else:
                return False

def topParent(particle):
    nP = particle.nParents()
    if nP == 0:
        return 0
    for parent_index in range(nP):
        parent = particle.parent(parent_index)
        if parent.pdgId() == 6:
            return 1
        elif parent.pdgId() == -6:
            return -1
        else:
            return topParent(parent)

def inAncestry(particle,ancestorPDG,allowAntiParticle=True,withStatus=0):
    nP = particle.nParents()
    if nP == 0: return 0.
    if allowAntiParticle: ancestorPDG = abs(ancestorPDG)
    for parent_index in range(nP):
        parent = particle.parent(parent_index)
        pPDG = parent.absPdgId() if allowAntiParticle else parent.pdgId()
        status = (not withStatus) or (parent.status()==withStatus)
        if pPDG == ancestorPDG and status:
            return parent.pt()
        else:
            parent_pt = inAncestry(parent,ancestorPDG,allowAntiParticle,withStatus)
            if parent_pt>0.: 
                return parent_pt
    return 0.

file_pre = "/data_ceph/cburton/MCProduction/ttbar/large/"
file_170 = []
file_170.append("MC15.999991.PhPy8EG_tt_mtop170p0_nonallhad_B2Jpsimumu.root")
file_170.append("MC15.999990.PhPy8EG_tt_mtop170p0_nonallhad_AntiB2Jpsimumu.root")
file_175 = []
file_175.append("MC15.999998.PhPy8EG_tt_mtop175p0_nonallhad_AntiB2Jpsimumu.root")
file_175.append("MC15.999999.PhPy8EG_tt_mtop175p0_nonallhad_B2Jpsimumu.root")
file_list = [file_170,file_175]
resp_top_mass = [170.0,175.0]

for d_ind in range(2):

    out_file = open("data_"+str(d_ind)+"_truth_300.dat","w")
    out_file.write("top_mass, m_total, Jpsi_E, Jpsi_px, Jpsi_py, Jpsi_pz, l_E, l_px, l_py, l_pz, is_elec\n")
    
    for file_name in file_list[d_ind]:
        x = 0
        f = ROOT.TFile.Open(file_pre+file_name)
        t = ROOT.xAOD.MakeTransientTree(f,'CollectionTree')

        for event in xrange(t.GetEntries()):
            top_mass = resp_top_mass[d_ind]
            t.GetEntry(event)
            for t1 in t.TruthParticles:
                if (t1.pdgId()==443):
                    j4vec = ROOT.TLorentzVector()
                    j4vec.SetPtEtaPhiM(t1.pt(),t1.eta(),t1.phi(),t1.m())

                    for t2 in t.TruthParticles: 
                        if((abs(t2.pdgId()) == 13 or abs(t2.pdgId()) == 11) and \
                            abs(topParent(t2)) == 1 and \
                            t2.pt() > 20000 and not \
                            inAncestry(t2,443,False)):
                            
                            lep4vec = ROOT.TLorentzVector()
                            lep4vec.SetPtEtaPhiM(t2.pt(),t2.eta(),t2.phi(),t2.m())

                            inv_mass = (j4vec+lep4vec).M()*0.001
                            if ((inv_mass >= 10) and (inv_mass <= 300)):
                                px_J, py_J, pz_J, E_J = j4vec.Px(), j4vec.Py(), j4vec.Pz(), j4vec.E()
                                px_l, py_l, pz_l, E_l = lep4vec.Px(), lep4vec.Py(), lep4vec.Pz(), lep4vec.E()
                                is_electron = True if (abs(t2.pdgId()) == 13) else False
                                out_file.write("%f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f\n" %(top_mass,inv_mass, E_J,px_J,py_J,pz_J,E_l,px_l,py_l,pz_l,is_electron))
                                x += 1
            if x > 50000:
                break

        ROOT.xAOD.ClearTransientTrees()
        f.Close()
    out_file.close()
