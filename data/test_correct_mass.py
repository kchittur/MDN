import ROOT
from math import cos, sin, sinh
import os.path


out_file = open("data_test.dat","w")
out_file.write("top_mass, Jpsi_E, Jpsi_px, Jpsi_py, Jpsi_pz, l_E, l_px, l_py, l_pz, is_elec\n")

f_path = "user.cburton.13232989._000001.output.root"
#if not os.path.isfile(f_path):
    #break
f = ROOT.TFile(f_path)
t = f.Get("nominal")


x = 0

for e in t:
    
    top_mass = 170.0 

    px_J, py_J = e.Jpsi_Pt[0]*cos(e.Jpsi_Phi[0]),  e.Jpsi_Pt[0]*sin(e.Jpsi_Phi[0])
    pz_J = e.Jpsi_Pt[0]*sinh(e.Jpsi_Eta[0])
    E_J = e.Jpsi_E[0]

    if len(e.electron_eta) == 1:
        px_l, py_l = e.electron_pt[0]*cos(e.electron_phi[0]),  e.electron_pt[0]*sin(e.electron_phi[0])
        pz_l = e.electron_pt[0]*sinh(e.electron_eta[0])
        E_l = e.electron_E[0]
        is_electron = True
    elif len(e.muon_eta) == 1:
        px_l, py_l = e.muon_pt[0]*cos(e.muon_phi[0]),  e.muon_pt[0]*sin(e.muon_phi[0])
        pz_l = e.muon_pt[0]*sinh(e.muon_eta[0])
        E_l = e.muon_E[0]
        is_electron = False
    else:
        continue


    out_file.write("%f, %f, %f, %f, %f, %f, %f, %f, %f %s\n" %(top_mass,E_J,px_J,py_J,pz_J,E_l,px_l,py_l,pz_l,is_electron))

    x += 1
    if x > 100000:
        continue       

out_file.close()
