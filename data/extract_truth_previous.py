import ROOT
from math import cos, sin, sinh, cosh, sqrt
import os
'''
410493: 170
410494: 175
'''

file_ind = [410493, 410494]
resp_top_mass = [170.0,175.0]

for d_ind in range(2):

    out_file = open("data_"+str(d_ind)+"_truth_prev.dat","w")
    out_file.write("top_mass, m_total, Jpsi_E, Jpsi_px, Jpsi_py, Jpsi_pz, l_E, l_px, l_py, l_pz, is_elec\n")
    d = file_ind[d_ind]
    f_path_pre = "/data_ceph/cburton/ntuples/"+str(d) 
    for file in os.listdir(f_path_pre):
        if not file.endswith(".root"):
            continue
        f = ROOT.TFile(f_path_pre+'/'+file)
        t = f.Get("nominal")

        for t1 in t:
            top_mass = resp_top_mass[d_ind]
            for j_mass,j_pt,j_eta,j_phi,j_pdgId,j_top in zip(t1.m_truth_m,t1.m_truth_pt,t1.m_truth_eta,t1.m_truth_phi,t1.m_truth_pdgId,t1.m_truth_topParent):
                if j_pdgId==443:
                    px_J, py_J = j_pt*cos(j_phi),  j_pt*sin(j_phi)
                    pz_J = j_pt*sinh(j_eta)
                    E_J = sqrt(j_mass**2 + (j_pt*cosh(j_eta))**2)


                    for m_mass,m_pt,m_eta,m_phi,m_pdgId,m_top,m_parents in zip(t1.m_truth_m,t1.m_truth_pt,t1.m_truth_eta,t1.m_truth_phi,t1.m_truth_pdgId,t1.m_truth_topParent,t1.m_truth_parents):
                        if((m_pdgId == 13) or (m_pdgId == 11)):
                            px_l, py_l = m_pt*cos(m_phi), m_pt*sin(m_phi)
                            pz_l = m_pt*sinh(m_eta)
                            E_l = sqrt(m_mass**2 + (m_pt*cosh(m_eta))**2)
                            is_electron = True if (m_pdgId == 13) else False

                            p_total_sq = (px_J+px_l)**2 + (py_J+py_l)**2 + (pz_J+pz_l)**2
                            m_total = (E_J+E_l)**2 + p_total_sq
                            m_total = sqrt(m_total)


                            out_file.write("%f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f\n" %(top_mass,m_total, E_J,px_J,py_J,pz_J,E_l,px_l,py_l,pz_l,is_electron))
    out_file.close()

