# top_mass, Jpsi_E, Jpsi_px, Jpsi_py, Jpsi_pz, l_E, l_px, l_py, l_pz

import numpy as np
from math import sqrt

for i in range(0,2):
    data_0_truth_300.dat
    in_file = open("../data_"+str(i)+"_truth_300.dat","r")
    out_file = open("single_data_"+str(i)+"_truth_300.dat","w")
    out_file.write("top_mass, mass_tot, mass_Jpsi, mass_l\n")

    k = 0 
    for line in in_file:
        top_mass, Jpsi_E, Jpsi_px, Jpsi_py, Jpsi_pz, l_E, l_px, l_py, l_pz = line.split(',')
        if top_mass == "top_mass":
            continue 
        [top_mass, Jpsi_E, Jpsi_px, Jpsi_py, Jpsi_pz, l_E, l_px, l_py, l_pz] = [float(x) for x in [top_mass, Jpsi_E, Jpsi_px, Jpsi_py, Jpsi_pz, l_E, l_px, l_py, l_pz]]
        mass_Jpsi = sqrt(Jpsi_E**2 - Jpsi_px**2 - Jpsi_py**2 - Jpsi_pz**2)
        mass_l = (l_E**2 - l_px**2 - l_py**2 - l_pz**2)
        if mass_l < 0.0:
            print("%f %f %f %f %f\n" % (top_mass, l_E,l_px,l_py,l_pz))
        mass_tot = (Jpsi_E + l_E)**2 - (Jpsi_px + l_px)**2 - (Jpsi_py + l_py)**2 - (Jpsi_pz + l_pz)**2 
        mass_tot = sqrt(mass_tot)    
        out_file.write("%f %f %f %f\n" % (top_mass, mass_tot, mass_Jpsi, mass_l))
        if k >50:
            break
        k+=1
    in_file.close()
    out_file.close()
