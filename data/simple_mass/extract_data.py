import ROOT
from math import cos, sin, sinh, sqrt
import os.path

file_ind = [410037, 410038, 410039, 410040, 410041]
resp_top_mass = [170.0,171.5,173.5,175.0,177.5]

for d_ind in range(5):

    out_file = open("data_"+str(d_ind)+".dat","w")
    out_file.write("top_mass, m_tot, m_Jpsi, m_lep")

    for i in range(1,11):
        d = file_ind[d_ind]
        f_path = "/data/kartik/" + str(d) + "/" + str(i) + ".root"
        if not os.path.isfile(f_path):
            break
        f = ROOT.TFile(f_path)
        t = f.Get("nominal")


        x = 0

        for e in t:
            
            top_mass = resp_top_mass[d_ind]

            px_J, py_J = e.Jpsi_Pt[0]*cos(e.Jpsi_Phi[0]),  e.Jpsi_Pt[0]*sin(e.Jpsi_Phi[0])
            pz_J = e.Jpsi_Pt[0]*sinh(e.Jpsi_Eta[0])
            E_J = e.Jpsi_E[0]

            if len(e.electron_eta) == 1:
                px_l, py_l = e.electron_pt[0]*cos(e.electron_phi[0]),  e.electron_pt[0]*sin(e.electron_phi[0])
                pz_l = e.electron_pt[0]*sinh(e.electron_eta[0])
                E_l = e.electron_E[0]
                is_electron = True
            elif len(e.muon_eta) == 1:
                px_l, py_l = e.muon_pt[0]*cos(e.muon_phi[0]),  e.muon_pt[0]*sin(e.muon_phi[0])
                pz_l = e.muon_pt[0]*sinh(e.muon_eta[0])
                E_l = e.muon_E[0]
                is_electron = False
            else:
                continue

            m_Jpsi = sqrt(E_J**2 - px_J**2 - py_J**2 - pz_J**2)
            m_l = sqrt(E_l**2 - px_l**2 - py_l**2 - pz_l**2)

            m_tot = (E_J + E_l)**2 - (px_J + px_l)**2 - (py_J + px_l)**2 - (pz_J + pz_l)**2
            m_tot = sqrt(m_tot)
            
            out_file.write("%f, %f, %f, %f, %f, %f, %f, %f, %f\n" %(top_mass,m_tot,m_Jpsi,m_l))

            x += 1
            if x > 100000:
                continue       

    out_file.close()
