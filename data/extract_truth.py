import ROOT
from math import cos, sin, sinh, cosh, sqrt
import os

def lepton_from_Jpsi(parent_list,m_truth_pdgId,m_truth_barcode):
    for parent_barcode in parent_list:
        for truth_barcode,truth_pdgId in zip(m_truth_barcode,m_truth_pdgId):
            if truth_barcode==parent_barcode and truth_pdgId==443:
                return True
            else:
                return False


file_ind = [410493, 410494]
resp_top_mass = [170.0,175.0]

for d_ind in range(2):

    out_file = open("data_"+str(d_ind)+"_truth_prev_150.dat","w")
    out_file.write("top_mass, m_total, Jpsi_E, Jpsi_px, Jpsi_py, Jpsi_pz, l_E, l_px, l_py, l_pz, is_elec\n")
    d = file_ind[d_ind]
    f_path_pre = "/data/kartik/"+str(d)
    for file in os.listdir(f_path_pre):
        if not file.endswith(".root"):
            continue
        f = ROOT.TFile(f_path_pre+'/'+file)
        t = f.Get("nominal")

        for t1 in t:
            top_mass = resp_top_mass[d_ind]
            for j_mass,j_pt,j_eta,j_phi,j_pdgId,j_top in zip(t1.m_truth_m,t1.m_truth_pt,t1.m_truth_eta,t1.m_truth_phi,t1.m_truth_pdgId,t1.m_truth_topParent):
                if j_pdgId==443:
                    j4vec = ROOT.TLorentzVector()
                    j4vec.SetPtEtaPhiM(j_pt,j_eta,j_phi,j_mass)                    

                    for m_mass,m_pt,m_eta,m_phi,m_pdgId,m_top,m_parents in zip(t1.m_truth_m,t1.m_truth_pt,t1.m_truth_eta,t1.m_truth_phi,t1.m_truth_pdgId,t1.m_truth_topParent,t1.m_truth_parents):
                        if((abs(m_pdgId) == 13 or abs(m_pdgId) == 11) and \
                            abs(m_top) == 1 and \
                            m_pt > 20000 and not \
                            lepton_from_Jpsi(m_parents,t1.m_truth_pdgId,t1.m_truth_barcode)):
                            lep4vec = ROOT.TLorentzVector()
                            lep4vec.SetPtEtaPhiM(m_pt,m_eta,m_phi,m_mass)
                            inv_mass = (j4vec+lep4vec).M()*0.001
                            if ((inv_mass >= 10) and (inv_mass <= 150)):
                                px_J, py_J, pz_J, E_J = j4vec.Px(), j4vec.Py(), j4vec.Pz(), j4vec.E() 
                                px_l, py_l, pz_l, E_l = lep4vec.Px(), lep4vec.Py(), lep4vec.Pz(), lep4vec.E()
                                is_electron = True if (abs(m_pdgId) == 13) else False
                                out_file.write("%f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f\n" %(top_mass,inv_mass, E_J,px_J,py_J,pz_J,E_l,px_l,py_l,pz_l,is_electron)) 

    out_file.close()

